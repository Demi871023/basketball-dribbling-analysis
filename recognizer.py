# Copyright (c) OpenMMLab. All rights reserved.
import argparse
import time
from collections import deque
from operator import itemgetter
from threading import Thread
from unittest import result

import cv2
import numpy as np
import torch
from mmcv import Config, DictAction
from mmcv.parallel import collate, scatter

from mmaction.apis import init_recognizer
from mmaction.datasets.pipelines import Compose

# from mmaction.apis import init_recognizer
# from mmaction.datasets.pipelines import Compose

import configs

HIGHTLOCATION = 0

EXCLUED_STEPS = [
    'OpenCVInit', 'OpenCVDecode', 'DecordInit', 'DecordDecode', 'PyAVInit',
    'PyAVDecode', 'RawFrameDecode'
]

# Load TSM Official Checkpoints into MMAction2 
def resave_checkpoints(path):
    checkpoint = torch.load(path)
    checkpoint = checkpoint['state_dict']

    preweight = []
    modelweight = []

    # TSM Official
    for key in checkpoint.keys():
        preweight.append(key.replace("module.", ""))

    # MMAction2
    for key in model.state_dict().keys():
        modelweight.append(key)

    base_dict = {'.'.join(k.split('.')[1:]): v for k, v in list(checkpoint.items())}
    replace_dict = {}
    for i in range(len(preweight)):
        replace_dict[preweight[i]] = modelweight[i]     # TSM Official Key, MMAction2

    for k, v in replace_dict.items():
        if k in base_dict:
            base_dict[v] = base_dict.pop(k) #pop 出來 key == k 的 value

    model.load_state_dict(base_dict)
    torch.save(model.state_dict(), 'checkpoints/tsn_r50_256p_1x1x8_100e_kinetics400_rgb.pth')

def inference():
    global Model_Predict_Text
    score_cache = deque()
    scores_sum = 0
    cur_time = time.time()

    while True:

        cur_windows = []

        while len(cur_windows) == 0:
            if len(frame_queue) == sample_length:
                cur_windows = list(np.array(frame_queue))
                if data['img_shape'] is None:
                    data['img_shape'] = frame_queue.popleft().shape[:2]

        cur_data = data.copy()
        cur_data['imgs'] = cur_windows
        cur_data = test_pipeline(cur_data)
        cur_data = collate([cur_data], samples_per_gpu=1)
        if next(model.parameters()).is_cuda:
            cur_data = scatter(cur_data, [device])[0]

        with torch.no_grad():
            scores = model(return_loss=False, **cur_data)[0]


        score_cache.append(scores)
        scores_sum += scores

        if len(score_cache) == average_size:
            scores_avg = scores_sum / average_size
            num_selected_labels = min(len(label), 5)
            scores_tuples = tuple(zip(label, scores_avg))
            scores_sorted = sorted(
                scores_tuples, key=itemgetter(1), reverse=True)
            results = scores_sorted[:num_selected_labels]

            Model_Predict_Text = []
            SCORE = {}
            RANK = {}
            rank_count = 0
            for type, score in results:           
                SCORE[type] = str(score)
                RANK[type] = rank_count
                rank_count = rank_count + 1

            for type in configs.DRIBBLING_TYPE:
                Model_Predict_Text.append([SCORE[type], RANK[type]])

            result_queue.append(Model_Predict_Text)
            scores_sum -= score_cache.popleft()

        if inference_fps > 0:
            sleep_time = 1 / inference_fps - (time.time() - cur_time)
            if sleep_time > 0:
                time.sleep(sleep_time)
            cur_time = time.time()

def image_recognizer_main():
    global frame_queue, frame, results, threshold, sample_length,\
        data, test_pipeline, model, device, average_size, label, dribbling_type,\
        result_queue, drawing_fps, inference_fps

    # # Args Setting
    average_size = configs.AVERAGE_SIZE
    threshold = configs.THRESHOLD
    drawing_fps = configs.DRAWING_FPS
    inference_fps = configs.INFERENCE_FPS
    device = torch.device(configs.DEVICE)
    cfg = Config.fromfile(configs.MODEL_CONFIG)
    model = init_recognizer(cfg, configs.CHECKPOINT, device = device)
    data = dict(img_shape = None, modality = 'RGB', label = -1)
    with open(configs.LABEL, 'r') as f:
        label = [line.strip() for line in f]

    # prepare test pipeline from non-camera pipeline
    cfg = model.cfg
    sample_length = 0
    pipeline = cfg.data.test.pipeline
    pipeline_ = pipeline.copy()
    for step in pipeline:
        if 'SampleFrames' in step['type']:
            sample_length = step['clip_len'] * step['num_clips']
            data['num_clips'] = step['num_clips']
            data['clip_len'] = step['clip_len']
            pipeline_.remove(step)
        if step['type'] in EXCLUED_STEPS:
            pipeline_.remove(step)
    test_pipeline = Compose(pipeline_)

    assert sample_length > 0

    frame_queue = deque(maxlen = sample_length)
    result_queue = deque(maxlen=1)

