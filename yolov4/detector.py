from tkinter import Frame
from yolov4.tool.utils import *
from yolov4.tool.torch_utils import *
from yolov4.tool.darknet2pytorch import Darknet
import argparse
from yolov4.tool.draw_trajectory import *
import re
from pathlib import Path
import cv2
from collections import deque
import configs

use_cuda = True

def ball_detector_main():
    global YOLOv4, YOLOv4_class_names, YOLOv4_frame_queue, YOLOv4_result_list, YOLOv4_dribble_sequence, YOLOv4_dribble_count

    YOLOv4 = Darknet("yolov4/cfg/yolov4-obj.cfg")
    YOLOv4.load_weights("yolov4/weights/yolov4_finetune.weights")
    YOLOv4_class_names = load_class_names('yolov4/data/x.names')
    YOLOv4.cuda()
    YOLOv4_frame_queue = deque(maxlen = 500)
    YOLOv4_result_list = []
    YOLOv4_dribble_sequence = ""
    YOLOv4_dribble_count = 0


def detect_dribblewave(curID):

    global YOLOv4_dribble_sequence, YOLOv4_dribble_count
    
    p1_y = YOLOv4_result_list[curID]
    p2_y = YOLOv4_result_list[curID - 1]
    p3_y = YOLOv4_result_list[curID - 2]

    v1 = p1_y - p2_y
    v2 = p2_y - p3_y

    if (v1 * v2) < 0:
        if v1 > 0 and v2 < 0:   # 波谷
            wave_type = "low"
            YOLOv4_dribble_sequence = YOLOv4_dribble_sequence + "l"
        if v1 < 0 and v2 > 0:   # 波峰
            wave_type = "high"
            YOLOv4_dribble_sequence = YOLOv4_dribble_sequence + "h"

    dribbling_counting()

def Init_and_Clear():
    global YOLOv4_result_list, YOLOv4_dribble_sequence, YOLOv4_dribble_count, YOLOv4_frame_queue
    YOLOv4_result_list.clear()
    YOLOv4_dribble_sequence = ""
    YOLOv4_dribble_count = 0
    YOLOv4_frame_queue.clear()


def dribbling_counting():
    global YOLOv4_dribble_sequence, YOLOv4_dribble_count

    if configs.user_choosen_dribbling == 0:
        YOLOv4_dribble_count = len(re.findall('(?=hlh)', YOLOv4_dribble_sequence))
    elif configs.user_choosen_dribbling == 1:
        YOLOv4_dribble_count = len(re.findall('(?=hlhlh)', YOLOv4_dribble_sequence))
    elif configs.user_choosen_dribbling == 2:
        YOLOv4_dribble_count = len(re.findall('(?=hlhlh)', YOLOv4_dribble_sequence))
    elif configs.user_choosen_dribbling == 3:
        YOLOv4_dribble_count = len(re.findall('(?=hlhlh)', YOLOv4_dribble_sequence))

def inference():
    global YOLOv4_dribble_sequence 

    while True:
        if len(YOLOv4_frame_queue) > 0:
            img = YOLOv4_frame_queue.popleft()
            sized = cv2.resize(img, (YOLOv4.width, YOLOv4.height))
            sized = cv2.cvtColor(sized, cv2.COLOR_BGR2RGB)

            boxes = do_detect(YOLOv4, sized, 0.4, 0.6, use_cuda)

            if len(boxes[0]) > 0:
                point_1x, point_1y = boxes[0][0][0]*1280, boxes[0][0][1]*720
                point_2x, point_2y = boxes[0][0][2]*1280, boxes[0][0][3]*720
                bbox_centerx, bbox_centery = (point_1x + point_2x)/2, (point_1y + point_2y)/2

                YOLOv4_result_list.append(-bbox_centery)
                if len(YOLOv4_result_list) < 2:
                    pass
                else:
                    detect_dribblewave(len(YOLOv4_result_list)-1)

            else:
                YOLOv4_dribble_sequence = YOLOv4_dribble_sequence + "n"
                YOLOv4_result_list.append(0)

