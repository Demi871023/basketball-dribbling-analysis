# import cv2


# Server Config
SERVER_HOST = '127.0.0.1'
SERVER_PORT = 20000
LISTEN_NUM = 10
CLIENT_ADDRESS = ''


# RECOGNIZER_MODE = 0 → IMAGE
AVERAGE_SIZE = 5
THRESHOLD = 0.01
DRAWING_FPS = 20
INFERENCE_FPS = 4
DEVICE = "cuda:0"
MODEL_CONFIG = "Recognizer/model_config.py"
CHECKPOINT = "Recognizer/checkpoint.pth"
LABEL = "Recognizer/label.txt"

# Definiation
NOT_CHOOSE = -1

DRIBBLING_POUND = 0
DRIBBLING_CROSSOVER = 1
DRIBBLING_ONESIDELEG = 2
DRIBBLING_BEHIND = 3

# one side leg dribble

DRIBBLING_TYPE = ["Pound", "Cross Over", "Between the Legs", "Behind"]
DRIBBLING_MAPPING = {"Pound":0, "Cross Over":1, "Between the Legs":2, "Behind":3}

# Camera Visualize
# FONT_FACE = cv2.FONT_HERSHEY_COMPLEX_SMALL 
FONT_SCALE = 1
FONT_COLOR_NORMAL = (255, 255, 255)
FONT_COLOR_HIGHTLIGHT = (0, 255, 255)
FONT_COLOR_MSG = (128, 128, 128)
FONT_THICKNESS = 1
FONT_LINETYPE = 1

UI_TOP = 0
UI_LEFT = 0
UI_WIDTH = 1280
UI_HEIGHT = 720


user_choosen_dribbling = NOT_CHOOSE
