import sys
from tkinter import Frame

from torch import true_divide
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtChart import *
from PyQt5.Qt import Qt
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

import os
import cv2
from threading import Thread
import numpy as np

import configs
import recognizer as recognizer
# import VisualAttention as VisualAttention
import yolov4.detector as detector
# import SocketManger as SocketManger

class FrameManger():
    def __init__(self, parent=None):
        super(FrameManger, self).__init__()
        self.frames = []
        self.frame_num = 0


class MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.timer_camera = QtCore.QTimer()
        self.frame_manger = FrameManger()

        detector.ball_detector_main()
        thread_ball_detector = Thread(target= detector.inference, args= (), daemon= True)
        thread_ball_detector.start()

        recognizer.image_recognizer_main()
        thread_image_recognizer = Thread(target= recognizer.inference, args= (), daemon = True)
        thread_image_recognizer.start()

        self.UI_init()
        self.SLOT_init()

    def UI_init(self):
        self.setWindowTitle(u'Dribbling IMG Recognizer Analyzer')
        self.setGeometry(configs.UI_TOP, configs.UI_LEFT, configs.UI_WIDTH, configs.UI_HEIGHT)
        self.setStyleSheet("background-color: black;")

        # NOTE : Setup Widget
        self.text_filepath = QLineEdit()
        self.button_openfile = QPushButton(u'Open File')
        self.__layout_function = QtWidgets.QHBoxLayout()
        self.__layout_function.addWidget(self.text_filepath)
        self.__layout_function.addWidget(self.button_openfile)
        self.Ui_Function_Setting()

        self.__widget_MODELPREDICT = QTextEdit()
        self.Ui_MODELPREDICT_Initial()
        self.__widget_DRIBBLEDETAIL = QTextEdit()
        self.Ui_DRIBBLEDETAIL_Initial()

        # Camera
        self.__widget_CAMERA = QLabel()
        self.Ui_CAMERA_Initial()
        self.camera_slider = QSlider(Qt.Horizontal)
        self.Ui_Slider_Initial()

        # Curve Canvass
        self.__widget_CANVAS = pg.PlotWidget() 
        self.Ui_CANVAS_Initial()
        self.timeline = pg.InfiniteLine(angle = 90, movable = True, label='frame = {value:0.1f}', pen = 'r', hoverPen = 'w', labelOpts={'position':0.1, 'color':(255, 255, 255), 'fill':(255, 0, 0, 50)} )
        self.yvalueline = pg.InfiniteLine(angle = 0, movable = False, label='yvalue = {value:0.2f}', pen = 'y', labelOpts={'position':0.1, 'color':(255, 255, 255), 'fill':(255, 255, 0, 50)})
        self.__widget_CANVAS.addItem(self.timeline)
        self.__widget_CANVAS.addItem(self.yvalueline)
        self.curve = self.__widget_CANVAS.plot()
        self.Ui_CURVE_Initial()


        self.__layout_main = QtWidgets.QVBoxLayout()  
        self.__layout_INFOBOARD = QtWidgets.QHBoxLayout()
        self.__layout_DRIBBLETYPE = QtWidgets.QVBoxLayout()

        self.__layout_DRIBBLETYPE.addWidget(self.__widget_MODELPREDICT)


        self.__layout_INFOBOARD.addLayout(self.__layout_DRIBBLETYPE)
        self.__layout_INFOBOARD.addWidget(self.__widget_CAMERA)
        self.__layout_INFOBOARD.addWidget(self.__widget_DRIBBLEDETAIL)

        self.__layout_main.addLayout(self.__layout_function)
        self.__layout_main.addLayout(self.__layout_INFOBOARD)
        self.__layout_main.addWidget(self.__widget_CANVAS)
        

        self.setLayout(self.__layout_main)

    def Ui_CAMERA_Initial(self):
            self.__widget_CAMERA.setFixedSize(640, 360)
            self.__widget_CAMERA.setText("Camera Capture")
            self.__widget_CAMERA.setStyleSheet(
                "color: rgb(255,255,255);"
                "font:20px 'Bahnschrift';"
                "border: 1px solid white;"
            )

    def Ui_Slider_Initial(self):
        self.camera_slider.setRange(0,0)
        self.camera_slider.sliderMoved.connect(self.Ui_CANVAS_TIMELINE_DRAG)

    def Ui_CANVAS_TIMELINE_DRAG(self, position):
        self.timeline.setPos(position)
        self.yvalueline.setPos(detector.YOLOv4_result_list[position])
        frame = self.frame_manger.frames[position]
        frame = cv2.resize(frame, (640, 360))
        frameImage = QtGui.QImage(frame.data, frame.shape[1], frame.shape[0], QtGui.QImage.Format_BGR888)
        self.__widget_CAMERA.setPixmap(QtGui.QPixmap.fromImage(frameImage))

    def Ui_CURVE_Change(self, duration):
        self.camera_slider.setRange(0, duration)


    def SLOT_init(self):
        self.timer_camera.timeout.connect(self.Ui_Upper_SetResult)
        self.timeline.sigDragged.connect(self.Ui_TIMELINE_SigDragged)
        self.button_openfile.clicked.connect(self.open_file)

    def open_file(self):
        path, _ = QFileDialog.getOpenFileName(self, "Open Video")
        print("open file: " + path)
        if path != '':
            self.text_filepath.setText(path)
            self.Ui_CURVE_Initial()
            self.cap = cv2.VideoCapture(path)
            self.timeline.setPos(0)
            self.yvalueline.setPos(0)
            self.timer_camera.start(30)
            self.frame_manger = FrameManger()
        

    def Ui_Upper_SetResult(self):
        flag, frame = self.cap.read()
        if flag :
            frame = cv2.resize(frame, (640, 360))
            self.frame_manger.frames.append(frame)
            recognizer.frame_queue.append(np.array(frame[:, :, ::-1]))
            detector.YOLOv4_frame_queue.append(frame)

        if len(recognizer.result_queue) > 0:
            predict = recognizer.result_queue.popleft()
            self.__widget_MODELPREDICT.clear()

            for i, type in enumerate(configs.DRIBBLING_TYPE):
                predict_text = type + "\n  " + predict[i][0]

                if predict[i][1] == 0:
                    self.__widget_MODELPREDICT.setTextColor(QColor(13, 145, 196))
                else:
                    self.__widget_MODELPREDICT.setTextColor(QColor(173, 185, 202))

                self.__widget_MODELPREDICT.append(predict_text)

        if len(detector.YOLOv4_frame_queue) > 0:
            if len(detector.YOLOv4_result_list) > 0:
                frame = self.frame_manger.frames[len(detector.YOLOv4_result_list) - 1]
                frameImage = QtGui.QImage(frame.data, frame.shape[1], frame.shape[0], QtGui.QImage.Format_BGR888)
                self.__widget_CAMERA.setPixmap(QtGui.QPixmap.fromImage(frameImage))
                self.Ui_CURVE_Update()
                self.Ui_CURVE_Change(len(detector.YOLOv4_result_list) - 1)
                self.__widget_DRIBBLEDETAIL.clear()
                self.__widget_DRIBBLEDETAIL.append("Number of Dribble : \t" + str(detector.YOLOv4_dribble_count))

    def Ui_TIMELINE_SigDragged(self):
        currentPos = self.timeline.getPos()[0]

        if(currentPos < 0):
            nextPose = 0
        elif(currentPos > len(detector.YOLOv4_result_list) - 1):
            nextPose = len(detector.YOLOv4_result_list) - 1
        else:
            if(currentPos > int(currentPos)):
                nextPose = int(currentPos) + 1
            elif(currentPos < int(currentPos)):
                nextPose = int(currentPos) - 1

        self.timeline.setPos(nextPose)
        self.yvalueline.setPos(detector.YOLOv4_result_list[nextPose])

        frame = self.frame_manger.frames[nextPose]
        frame = cv2.resize(frame, (640, 360))
        frameImage = QtGui.QImage(frame.data, frame.shape[1], frame.shape[0], QtGui.QImage.Format_BGR888)
        self.__widget_CAMERA.setPixmap(QtGui.QPixmap.fromImage(frameImage))

    # Open File Function initialization...
    def Ui_Function_Setting(self):
        self.button_openfile.setStyleSheet(
            "QPushButton{color:black}"
            "QPushButton:hover{color:black}"
            "QPushButton{background-color:rgb(255,255,255)}"
            "QPushButton:hover{background-color:rgb(200, 200, 200)}"
            "QPushButton{font:12px 'Bahnschrift'}"
        )
        self.button_openfile.setEnabled(True)

        self.text_filepath.setStyleSheet(
            "QLineEdit{background-color:rgb(255, 255, 255)}"
            "QLineEdit{font:12px 'Bahnschrift'}"
        )
        self.text_filepath.setReadOnly(True)

    # Canvas initialization...
    def Ui_CANVAS_Initial(self):
        self.__widget_CANVAS.showGrid(x=False, y = False)
        self.__widget_CANVAS.setLabels(
            bottom = ' Time (frame) ',
            left = 'Ball Y Value (coordinate in window)'
        )
        self.__widget_CANVAS.setRange(yRange = [-0, -720])

    def Ui_CURVE_Initial(self):
        detector.Init_and_Clear()
        self.curve.setData(detector.YOLOv4_result_list)

    def Ui_CURVE_Update(self):
        self.curve.setData(detector.YOLOv4_result_list)
        self.timeline.setPos(len(detector.YOLOv4_result_list)-1)
        self.yvalueline.setPos(detector.YOLOv4_result_list[len(detector.YOLOv4_result_list)-1])

    def Ui_MODELPREDICT_Initial(self):
        self.__widget_MODELPREDICT.clear()
        self.__widget_MODELPREDICT.setReadOnly(True)
        self.__widget_MODELPREDICT.setStyleSheet(
            "color:rgb(173,185,202);"
            "font:20px 'Bahnschrift';"
            "background-color: rgb(51, 63, 80);"
        )

    def Ui_DRIBBLEDETAIL_Initial(self):
        self.__widget_DRIBBLEDETAIL.clear()
        self.__widget_DRIBBLEDETAIL.setReadOnly(True)
        self.__widget_DRIBBLEDETAIL.setStyleSheet(
            "color:rgb(200,200,200);"
            "font:20px 'Bahnschrift';"
            "background-color: rgb(38, 38, 38);"
        )

if __name__ == '__main__':
    configs.user_choosen_dribbling = configs.DRIBBLING_POUND
    App = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(App.exec_())