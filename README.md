# Basketball Dribbling Analysis

## Description
This user interface is facilitated to demonstrate the result from ball detection and action recognition for a video.

## Directory Structure
    .
    ├── README.md
    ├── requirements.txt
    ├── main.py    
    ├── configs.py
    ├── rcognizer.py
    ├── data
    │   └── video.mp4
    ├── yolov4
    │   ├── BallDector.py
    │   ├── cfg  
    │   ├── data
    │   └── weights
    └── Recognizer
        ├── checkpoint.pth
        ├── label.txt
        ├── model_config.py
        └── model_tsmr50.py

## Demo
![image](https://gitlab.com/Demi871023/basketball-dribbling-analysis/-/raw/main/demo.gif)